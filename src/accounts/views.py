from django.contrib.auth.views import LoginView, LogoutView
from django.shortcuts import render
from django.urls import reverse_lazy, reverse


class AccountLoginView(LoginView):
    success_url = reverse_lazy('index')
    template_name = 'login.html'

    def get_redirect_url(self):
        if self.request.GET.get('next'):
            return self.request.GET.get('next')
        return reverse('index')


class AccountLogoutView(LogoutView):
    template_name = 'logout.html'
