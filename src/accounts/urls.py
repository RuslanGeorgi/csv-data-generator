from django.urls import path

from accounts.views import AccountLoginView, AccountLogoutView

app_name = 'accounts'

urlpatterns = [
    path('login', AccountLoginView.as_view(), name='login'),
    path('logout', AccountLogoutView.as_view(), name='logout'),
]
