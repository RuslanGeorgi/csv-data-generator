from django.urls import path

from schemas.views import SchemaListView, SchemaDeleteView, SchemaCreateView

app_name = 'schemas'

urlpatterns = [
    path('', SchemaListView.as_view(), name='list'),
    path('delete/<int:id>', SchemaDeleteView.as_view(), name='delete'),
    path('create', SchemaCreateView.as_view(), name='create'),
]
