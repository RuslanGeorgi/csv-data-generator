from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import render
from django.urls import reverse_lazy
from django.views.generic import ListView, DeleteView, CreateView

from schemas.forms import SchemaCreateForm
from schemas.models import Schema


class SchemaListView(LoginRequiredMixin, ListView):
    model = Schema
    template_name = 'schemas_list.html'
    context_object_name = 'schemas'
    login_url = reverse_lazy('accounts:login')


class SchemaDeleteView(LoginRequiredMixin, DeleteView):
    model = Schema
    success_url = reverse_lazy('schemas:list')
    template_name = 'schemas_delete.html'
    pk_url_kwarg = 'id'
    login_url = reverse_lazy('accounts:login')


class SchemaCreateView(LoginRequiredMixin, CreateView):
    model = Schema
    form_class = SchemaCreateForm
    success_url = reverse_lazy('schemas:list')
    template_name = 'schemas_create.html'
    login_url = reverse_lazy('accounts:login')
