from django.db import models


class Schema(models.Model):
    title = models.CharField(max_length=64)
    modify_date = models.DateTimeField(null=True, auto_now=True)
