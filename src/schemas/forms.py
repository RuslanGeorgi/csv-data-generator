from django.forms import ModelForm

from schemas.models import Schema


class SchemaCreateForm(ModelForm):
    class Meta:
        model = Schema
        fields = '__all__'
