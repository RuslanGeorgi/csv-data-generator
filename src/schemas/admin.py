from django.contrib import admin

from schemas.models import Schema

admin.site.register(Schema)
